#部署对象和流程定义相关表
#当key值相同的时候版本升级，id未key+版本+随机生成的值
SELECT * FROM `act_re_deployment`; #部署对象表

SELECT * FROM `act_re_procdef`; #流程定义表`fsop`

SELECT * FROM `act_ge_bytearray`; #资源文件表（一个存储xml，一个存储图片）

SELECT * FROM `act_ge_property`; #逐渐生成策略表（与Id相关）

#####################################################
#流程实例，执行对象，任务
SELECT * FROM `act_ru_execution`; #正在执行的执行对象表（正在执行的流程实例）

SELECT * FROM `act_hi_procinst` WHERE `END_TIME_` IS NULL; #流程实例的历史表（一个流程实例）

SELECT * FROM `act_ru_task`; #正在执行的任务表（只有节点是UserTask的才有数据）

SELECT * FROM `act_hi_taskinst`; #任务历史表（只有节点是UserTask的时候该表存在数据）

SELECT * FROM `act_hi_actinst`; #所有活动节点的历史表（其中包括任务也不包括任务）
#####################################################
#流程变量
SELECT * FROM `act_ru_variable`;#正在执行的流程变量表

SELECT * FROM `act_hi_varinst`; #历史流程变量表

##################################################################
SELECT * FROM `act_ru_identitylink` #任务表（个人任务、组任务）

SELECT * FROM `act_hi_identitylink` #任务历史表

#########################################################
SELECT * FROM `act_id_group` #角色表

SELECT * FROM `act_id_user` #用户表

SELECT * FROM `act_id_membership` #用户角色关联表




